package main

import (
	"fmt"
	"github.com/TutorialEdge/beginners-guide-project/internal/network"
)

func main() {
	fmt.Println("Awesome CLI v0.0.1")
	network.Ping("127.0.0.1")
}
