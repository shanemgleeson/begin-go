Beginner's Project
==================

## Running Locally

If you ant to run this application locally then run the following command.

```bash
$ go run cmd/cli/main.go
```

## Build Application

```bash
$ go build -o cli cmd/cli/main.go
```

